package com.tarkshala.dukan.notification;

public interface Publisher {

    void publish(String message);
}

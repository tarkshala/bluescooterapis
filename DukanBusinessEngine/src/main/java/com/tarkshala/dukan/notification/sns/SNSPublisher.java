package com.tarkshala.dukan.notification.sns;

import com.amazonaws.services.sns.AmazonSNSAsyncClient;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.tarkshala.dukan.config.Constants;
import com.tarkshala.dukan.notification.Publisher;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class SNSPublisher implements Publisher {

    @Autowired
    private AmazonSNSAsyncClient snsAsyncClient;

    @Autowired
    private Logger logger;

    @Override
    public void publish(String message) {
        PublishRequest request = new PublishRequest(Constants.CREATE_ORDER_SNS_ARN, message);
        PublishResult result = snsAsyncClient.publish(request);
        logger.info(result.toString());
    }
}

package com.tarkshala.dukan.config;

public class Constants {

    public static final String BUSINESS_MODEL_PACKAGE_NAME = "com.tarkshala.dukan.business.model";
    public static final String DATA_MODEL_PACKAGE_NAME = "com.tarkshala.dukan.dao.model";
    public static final String DATA_OBJECT = "DO";
    public static final String BUSINESS_OBJECT = "BO";

    public static final String CATEGORY_DAO = "CategoryDao";
    public static final String PRODUCT_DAO = "ProductDao";

    public static final String CATEGORY_CONVERTER = "CategoryConverter";
    public static final String PRODUCT_CONVERTER = "ProductConverter";

    public static final String UID = "uid";

    public static final String ORDER_TRANSIT_TABLE_NAME = "bs-ORDERS-TRANSIT";
    public static final String OBSOLETE_ORDER_TABLE_NAME = "bs-ORDERS";

    public static final String CREATE_ORDER_SNS_ARN = "arn:aws:sns:ap-south-1:352095155554:bs-CREATE_ORDER";

}

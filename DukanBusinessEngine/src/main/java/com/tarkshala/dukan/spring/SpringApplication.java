package com.tarkshala.dukan.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Boiler plate code for Spring application.
 * The class provides boiler plate setup for a LambdaFunctionApplication.
 */
public class SpringApplication {

    private static ApplicationContext springContext;

    /**
     * Get the bean instance of {@link Class} type clazz from spring context.
     * It also initializes the beans if not present, thus it adds cold start latency for first request
     * to application. Subsequent calls will be pretty faster.
     *
     * @param clazz {@link Class} type
     * @param <T>
     * @return bean instance of given {@link Class} type
     */
    public static <T> T getBean(Class<T> clazz) {
        return getSpringContext().getBean(clazz);
    }

    private static ApplicationContext getSpringContext() {
        if (springContext == null) {
            synchronized (ApplicationContext.class) {
                if (springContext == null) {
                    springContext = new AnnotationConfigApplicationContext(SpringAppConfig.class);
                }
            }
        }
        return springContext;
    }
}

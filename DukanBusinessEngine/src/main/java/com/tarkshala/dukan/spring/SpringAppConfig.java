package com.tarkshala.dukan.spring;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.sns.AmazonSNSAsyncClient;
import com.amazonaws.services.sns.AmazonSNSAsyncClientBuilder;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tarkshala.dukan.business.model.filter.Filter;
import com.tarkshala.dukan.config.Constants;
import com.tarkshala.dukan.dao.OrderDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

import java.util.HashMap;
import java.util.Map;

/**
 * Spring application configuration, It is one of the place where beans can be initialized manually.
 * Other place is spring-config.xml, which is loaded as resource into this config object.
 */
@Configuration
@ImportResource("classpath:spring-config.xml")
public class SpringAppConfig {

    @Bean
    public AmazonDynamoDBClientBuilder getAmazonDynamoDBClientBuilder() {
        return AmazonDynamoDBClientBuilder.standard().withRegion(Regions.AP_SOUTH_1);
    }

    @Bean
    public DynamoDBMapper getDynamoDBMapper(final AmazonDynamoDBClientBuilder builder) {
        return new DynamoDBMapper(builder.build());
    }

    @Bean
    public DynamoDB getDynamoDB(final AmazonDynamoDBClientBuilder builder) {
        return new DynamoDB(builder.build());
    }

    @Bean
    public Gson getGson() {
        GsonBuilder builder = new GsonBuilder();
        return builder.setPrettyPrinting().create();
    }

    @Bean
    public Logger getLogger() {
        return LoggerFactory.getLogger("BlueScooterAPIs");
    }

    @Bean
    public Map<Filter.Operation, ComparisonOperator> getOperatorMapping() {

        Map<Filter.Operation, ComparisonOperator> operatorMap = new HashMap<>();

        // CONFIGURATION: Business Comparision Operator and DAO operators mapping
        operatorMap.put(Filter.Operation.EQ, ComparisonOperator.EQ);
        operatorMap.put(Filter.Operation.STARTS_WITH, ComparisonOperator.BEGINS_WITH);
        operatorMap.put(Filter.Operation.CONTAINS, ComparisonOperator.CONTAINS);

        return operatorMap;
    }

    @Bean
    public AmazonSNSClientBuilder getAmazonSNSClientBuilder() {
        return AmazonSNSClientBuilder.standard().withRegion(Regions.AP_SOUTH_1);
    }

    @Bean
    public AmazonSNSAsyncClientBuilder getAmazonSNSAsyncClientBuilder() {
        return AmazonSNSAsyncClientBuilder.standard().withRegion(Regions.AP_SOUTH_1);
    }

    @Bean
    public AmazonSNSClient getAmazonSNSClient(final AmazonSNSClientBuilder builder) {
        return  (AmazonSNSClient) builder.build();
    }

    @Bean
    public AmazonSNSAsyncClient getAmazonSNSAsyncClient(final AmazonSNSAsyncClientBuilder builder) {
        return (AmazonSNSAsyncClient) builder.build();
    }

    @Bean(name = Constants.ORDER_TRANSIT_TABLE_NAME)
    public OrderDAO getOrderDAO() {
        OrderDAO orderDAO = new OrderDAO();
        orderDAO.setTableName(Constants.ORDER_TRANSIT_TABLE_NAME);
        return orderDAO;
    }

    @Bean(name = Constants.OBSOLETE_ORDER_TABLE_NAME)
    public OrderDAO getOrderDAO2() {
        OrderDAO orderDAO = new OrderDAO();
        orderDAO.setTableName(Constants.OBSOLETE_ORDER_TABLE_NAME);
        return orderDAO;
    }
}

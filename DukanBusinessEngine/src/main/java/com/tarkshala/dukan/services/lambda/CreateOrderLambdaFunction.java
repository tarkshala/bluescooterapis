package com.tarkshala.dukan.services.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.google.gson.Gson;
import com.tarkshala.dukan.business.model.CreateOrderRequest;
import com.tarkshala.dukan.business.model.CreateOrderResponse;
import com.tarkshala.dukan.business.model.bo.OrderBO;
import com.tarkshala.dukan.dao.OrderDAO;
import com.tarkshala.dukan.dao.model.OrderDO;
import com.tarkshala.dukan.dao.model.convert.Converter;
import com.tarkshala.dukan.notification.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CreateOrderLambdaFunction implements RequestHandler<CreateOrderRequest, CreateOrderResponse> {

    @Autowired
    private OrderDAO orderDAO;

    @Autowired
    private Publisher publisher;

    @Autowired
    private Gson gson;

    @Autowired
    private Converter<OrderBO, OrderDO> converter;

    @Override
    public CreateOrderResponse handleRequest(CreateOrderRequest createOrderRequest, Context context) {
        OrderBO orderBO = createOrderRequest.getOrder();
        OrderDO order = converter.convertToDO(orderBO);
        orderDAO.save(order);
        publisher.publish(gson.toJson(order));

        return new CreateOrderResponse("Order is created successfully");
    }
}

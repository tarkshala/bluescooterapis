package com.tarkshala.dukan.services.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.tarkshala.dukan.business.model.CreateCategoryRequest;
import com.tarkshala.dukan.business.model.CreateCategoryResponse;
import com.tarkshala.dukan.spring.SpringApplication;

public class CreateCategoryLambdaFunctionApplication
        extends SpringApplication implements RequestHandler<CreateCategoryRequest, CreateCategoryResponse>  {
    @Override
    public CreateCategoryResponse handleRequest(CreateCategoryRequest createCategoryRequest, Context context) {
        return getBean(CreateCategoryLambdaFunction.class).handleRequest(createCategoryRequest, context);
    }
}

package com.tarkshala.dukan.services.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.tarkshala.dukan.business.model.CreateOrderRequest;
import com.tarkshala.dukan.business.model.CreateOrderResponse;
import com.tarkshala.dukan.spring.SpringApplication;

public class CreateOrderLambdaFunctionApplication
        extends SpringApplication implements RequestHandler<CreateOrderRequest, CreateOrderResponse> {

    @Override
    public CreateOrderResponse handleRequest(CreateOrderRequest createOrderRequest, Context context) {
        return getBean(CreateOrderLambdaFunction.class).handleRequest(createOrderRequest, context);
    }
}

package com.tarkshala.dukan.services.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.tarkshala.dukan.business.model.CreateProductRequest;
import com.tarkshala.dukan.business.model.CreateProductResponse;
import com.tarkshala.dukan.spring.SpringApplication;

public class CreateProductLambdaFunctionApplication
        extends SpringApplication
        implements RequestHandler<CreateProductRequest, CreateProductResponse> {

    @Override
    public CreateProductResponse handleRequest(CreateProductRequest createProductRequest, Context context) {
        return getBean(CreateProductLambdaFunction.class).handleRequest(createProductRequest, context);
    }
}

package com.tarkshala.dukan.services.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.tarkshala.dukan.business.model.FetchOrdersRequest;
import com.tarkshala.dukan.business.model.FetchOrdersResponse;
import com.tarkshala.dukan.spring.SpringApplication;

public class FetchTransitOrdersLambdaFunctionApplication extends SpringApplication
        implements RequestHandler<FetchOrdersRequest, FetchOrdersResponse> {

    @Override
    public FetchOrdersResponse handleRequest(FetchOrdersRequest fetchOrdersRequest, Context context) {
        return getBean(FetchTransitOrdersLambdaFunction.class).handleRequest(fetchOrdersRequest, context);
    }
}

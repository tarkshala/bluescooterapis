package com.tarkshala.dukan.services.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.google.gson.Gson;
import com.tarkshala.dukan.business.model.FetchOrdersRequest;
import com.tarkshala.dukan.business.model.FetchOrdersResponse;
import com.tarkshala.dukan.business.model.bo.OrderBO;
import com.tarkshala.dukan.config.Constants;
import com.tarkshala.dukan.dao.OrderDAO;
import com.tarkshala.dukan.dao.model.OrderDO;
import com.tarkshala.dukan.dao.model.convert.OrderConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class FetchTransitOrdersLambdaFunction implements RequestHandler<FetchOrdersRequest, FetchOrdersResponse> {

    @Autowired
    @Qualifier(Constants.ORDER_TRANSIT_TABLE_NAME)
    private OrderDAO orderDAO;

    @Autowired
    private OrderConverter converter;

    @Autowired
    private Gson gson;

    @Override
    public FetchOrdersResponse handleRequest(FetchOrdersRequest fetchOrdersRequest, Context context) {

        int pageSize = fetchOrdersRequest.getPageSize() == null ? Integer.MAX_VALUE : fetchOrdersRequest.getPageSize();
        List<OrderDO> orderDOs  = orderDAO.get(fetchOrdersRequest.getFilters(), fetchOrdersRequest.getPreviousPageLastKey(), pageSize);
        List<OrderBO> orders = orderDOs.stream().map(orderDO -> converter.convertToBO(orderDO)).collect(Collectors.toList());
        return new FetchOrdersResponse(orders);
    }
}

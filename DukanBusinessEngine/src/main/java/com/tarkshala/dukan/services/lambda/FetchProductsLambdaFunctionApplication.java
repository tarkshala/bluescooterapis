package com.tarkshala.dukan.services.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.tarkshala.dukan.business.model.FetchProductsRequest;
import com.tarkshala.dukan.business.model.FetchProductsResponse;
import com.tarkshala.dukan.spring.SpringApplication;

public class FetchProductsLambdaFunctionApplication extends SpringApplication
        implements RequestHandler<FetchProductsRequest, FetchProductsResponse> {

    @Override
    public FetchProductsResponse handleRequest(FetchProductsRequest fetchProductsRequest, Context context) {
        return getBean(FetchProductsLambdaFunction.class).handleRequest(fetchProductsRequest, context);
    }
}

package com.tarkshala.dukan.services.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.tarkshala.dukan.business.model.bo.CategoryBO;
import com.tarkshala.dukan.business.model.CreateCategoryRequest;
import com.tarkshala.dukan.business.model.CreateCategoryResponse;
import com.tarkshala.dukan.config.Constants;
import com.tarkshala.dukan.dao.CategoryDAO;
import com.tarkshala.dukan.dao.model.CategoryDO;
import com.tarkshala.dukan.dao.model.convert.Converter;
import com.tarkshala.dukan.dao.model.convert.ConverterFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CreateCategoryLambdaFunction implements RequestHandler<CreateCategoryRequest, CreateCategoryResponse> {

    @Autowired
    private CategoryDAO categoryDAO;

    @Autowired
    private ConverterFactory converterFactory;

    @Override
    public CreateCategoryResponse handleRequest(CreateCategoryRequest createCategoryRequest, Context context) {

        Converter<CategoryBO, CategoryDO> converter = converterFactory.getConverter(Constants.CATEGORY_CONVERTER);
        CategoryDO categoryDO = converter.convertToDO(createCategoryRequest.getCategoryBO());
        categoryDAO.save(categoryDO);
        return new CreateCategoryResponse("Category created successfully");
    }
}

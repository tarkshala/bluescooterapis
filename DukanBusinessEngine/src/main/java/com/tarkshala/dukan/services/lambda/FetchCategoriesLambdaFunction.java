package com.tarkshala.dukan.services.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.tarkshala.dukan.business.model.bo.CategoryBO;
import com.tarkshala.dukan.business.model.FetchCategoriesRequest;
import com.tarkshala.dukan.business.model.FetchCategoriesResponse;
import com.tarkshala.dukan.config.Constants;
import com.tarkshala.dukan.dao.CategoryDAO;
import com.tarkshala.dukan.dao.model.CategoryDO;
import com.tarkshala.dukan.dao.model.convert.Converter;
import com.tarkshala.dukan.dao.model.convert.ConverterFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class FetchCategoriesLambdaFunction
        implements RequestHandler<FetchCategoriesRequest, FetchCategoriesResponse> {

    @Autowired
    private CategoryDAO categoryDAO;

    @Autowired
    private ConverterFactory converterFactory;

    @Override
    public FetchCategoriesResponse handleRequest(FetchCategoriesRequest fetchCategoriesRequest, Context context) {

        Converter<CategoryBO, CategoryDO> converter = converterFactory.getConverter(Constants.CATEGORY_CONVERTER);
        List<CategoryBO> categories = categoryDAO.getAll().stream().map(converter::convertToBO).collect(Collectors.toList());
        return new FetchCategoriesResponse(categories);
    }
}

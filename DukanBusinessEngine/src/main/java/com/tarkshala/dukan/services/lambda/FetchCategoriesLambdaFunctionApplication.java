package com.tarkshala.dukan.services.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.tarkshala.dukan.business.model.FetchCategoriesRequest;
import com.tarkshala.dukan.business.model.FetchCategoriesResponse;
import com.tarkshala.dukan.spring.SpringApplication;

public class FetchCategoriesLambdaFunctionApplication extends SpringApplication
        implements RequestHandler<FetchCategoriesRequest, FetchCategoriesResponse> {

    @Override
    public FetchCategoriesResponse handleRequest(FetchCategoriesRequest fetchCategoriesRequest, Context context) {
        return getBean(FetchCategoriesLambdaFunction.class).handleRequest(fetchCategoriesRequest, context);
    }
}

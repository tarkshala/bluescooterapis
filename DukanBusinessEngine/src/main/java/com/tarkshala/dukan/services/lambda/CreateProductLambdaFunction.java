package com.tarkshala.dukan.services.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.tarkshala.dukan.business.model.CreateProductRequest;
import com.tarkshala.dukan.business.model.CreateProductResponse;
import com.tarkshala.dukan.dao.ProductDAO;
import com.tarkshala.dukan.dao.model.ProductDO;
import com.tarkshala.dukan.dao.model.convert.ProductConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CreateProductLambdaFunction implements RequestHandler<CreateProductRequest, CreateProductResponse> {

    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private ProductConverter converter;

    @Override
    public CreateProductResponse handleRequest(CreateProductRequest createProductRequest, Context context) {

        ProductDO productDO = converter.convertToDO(createProductRequest.getProductBO());
        productDAO.save(productDO);
        return new CreateProductResponse("Product creation is successful");
    }
}

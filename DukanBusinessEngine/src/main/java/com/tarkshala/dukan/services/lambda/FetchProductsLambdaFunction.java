package com.tarkshala.dukan.services.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.google.gson.Gson;
import com.tarkshala.dukan.business.model.FetchProductsRequest;
import com.tarkshala.dukan.business.model.FetchProductsResponse;
import com.tarkshala.dukan.business.model.bo.ProductBO;
import com.tarkshala.dukan.dao.ProductDAO;
import com.tarkshala.dukan.dao.model.ProductDO;
import com.tarkshala.dukan.dao.model.convert.ProductConverter;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class FetchProductsLambdaFunction
        implements RequestHandler<FetchProductsRequest, FetchProductsResponse> {

    @Autowired
    private Logger logger;

    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private ProductConverter converter;

    @Override
    public FetchProductsResponse handleRequest(FetchProductsRequest fetchProductsRequest, Context context) {

        logger.info(new Gson().toJson(fetchProductsRequest));
        int count = fetchProductsRequest.getPageSize() == null? Integer.MAX_VALUE : fetchProductsRequest.getPageSize();
        List<ProductDO> productDOs = productDAO.get(fetchProductsRequest.getFilters(), fetchProductsRequest.getPreviousPageLastKey(), count);
        List<ProductBO> products = productDOs.stream().map(productDO -> converter.convertToBO(productDO)).collect(Collectors.toList());
        return new FetchProductsResponse(products);
    }
}

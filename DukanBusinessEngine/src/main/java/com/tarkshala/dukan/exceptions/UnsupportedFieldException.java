package com.tarkshala.dukan.exceptions;

public class UnsupportedFieldException extends RuntimeException {

    public UnsupportedFieldException() {
        super();
    }

    public UnsupportedFieldException(String message) {
        super(message);
    }

    public UnsupportedFieldException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnsupportedFieldException(Throwable cause) {
        super(cause);
    }

}

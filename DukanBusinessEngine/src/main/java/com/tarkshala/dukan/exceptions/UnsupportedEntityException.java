package com.tarkshala.dukan.exceptions;

public class UnsupportedEntityException extends RuntimeException {

    public UnsupportedEntityException() {
        super();
    }

    public UnsupportedEntityException(String message) {
        super(message);
    }

    public UnsupportedEntityException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnsupportedEntityException(Throwable cause) {
        super(cause);
    }


}

package com.tarkshala.dukan.dao.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@DynamoDBTable(tableName = "bs-ORDERS")
public class OrderDO extends EntityDO {

    @DynamoDBAttribute
    private long date;

    @DynamoDBAttribute
    private String buyerName;

    @DynamoDBAttribute
    private AddressDO address;

    @DynamoDBAttribute
    private List<ProductDO> products;

    @DynamoDBAttribute
    private String promotionalCode;
}

package com.tarkshala.dukan.dao.model.convert;

import com.google.gson.Gson;
import com.tarkshala.dukan.business.model.bo.AddressBO;
import com.tarkshala.dukan.dao.model.AddressDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AddressConverter implements Converter<AddressBO, AddressDO> {

    @Autowired
    private Gson gson;

    @Override
    public AddressBO convertToBO(AddressDO entityDO) {
        return gson.fromJson(gson.toJson(entityDO), AddressBO.class);
    }

    @Override
    public AddressDO convertToDO(AddressBO entityBO) {
        return gson.fromJson(gson.toJson(entityBO), AddressDO.class);
    }
}

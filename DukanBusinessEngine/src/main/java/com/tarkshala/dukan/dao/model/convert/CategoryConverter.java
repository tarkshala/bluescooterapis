package com.tarkshala.dukan.dao.model.convert;

import com.google.gson.Gson;
import com.tarkshala.dukan.business.model.bo.CategoryBO;
import com.tarkshala.dukan.config.Constants;
import com.tarkshala.dukan.dao.model.CategoryDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component(Constants.CATEGORY_CONVERTER)
public class CategoryConverter implements Converter<CategoryBO, CategoryDO> {

    @Autowired
    private Gson gson;

    @Override
    public CategoryBO convertToBO(CategoryDO entityDO) {
        String json = gson.toJson(entityDO);
        return gson.fromJson(json, CategoryBO.class);
    }

    @Override
    public CategoryDO convertToDO(CategoryBO entityBO) {
        String json = gson.toJson(entityBO);
        return gson.fromJson(json, CategoryDO.class);
    }
}

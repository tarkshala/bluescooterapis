package com.tarkshala.dukan.dao.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import lombok.*;

@DynamoDBDocument
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AddressDO extends EntityDO{

    @DynamoDBAttribute
    private String name;

    @DynamoDBAttribute
    private String email;

    @DynamoDBAttribute
    private String phone;

    @DynamoDBAttribute
    private String address;
}

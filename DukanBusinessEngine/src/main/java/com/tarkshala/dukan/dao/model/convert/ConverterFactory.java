package com.tarkshala.dukan.dao.model.convert;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class ConverterFactory {

    @Autowired
    private Map<String, Converter> converters;

    public Converter getConverter(String entityType) {
        return converters.get(entityType);
    }

}

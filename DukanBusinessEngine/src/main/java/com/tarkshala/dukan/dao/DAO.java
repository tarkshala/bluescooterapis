package com.tarkshala.dukan.dao;

import com.tarkshala.dukan.business.model.filter.Filter;

import java.util.List;
import java.util.Optional;

public interface DAO<T> {

    Optional<T> get(String id);

    List<T> getAll();

    List<T> get(List<Filter> filters, String previousPageLastKey, int count);

    void save(T entity);

    void update(T entity);

    void delete(T entity);
}

package com.tarkshala.dukan.dao;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.tarkshala.dukan.dao.model.EntityDO;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class EntityDAO<T extends EntityDO> implements DAO<T> {

    @Autowired
    private DynamoDBMapper dynamoDBMapper;

    @Override
    public void save(T entity) {
        dynamoDBMapper.save(entity);
    }

    @Override
    public void delete(T entity) {
        dynamoDBMapper.delete(entity);
    }

    @Override
    public void update(T entity) {
        dynamoDBMapper.save(entity);
    }
}

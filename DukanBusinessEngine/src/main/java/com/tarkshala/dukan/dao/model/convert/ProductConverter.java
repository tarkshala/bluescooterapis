package com.tarkshala.dukan.dao.model.convert;

import com.google.gson.Gson;
import com.tarkshala.dukan.business.model.bo.ProductBO;
import com.tarkshala.dukan.config.Constants;
import com.tarkshala.dukan.dao.model.ProductDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component(Constants.PRODUCT_CONVERTER)
public class ProductConverter implements Converter<ProductBO, ProductDO> {

    @Autowired
    private Gson gson;

    @Autowired
    private CategoryConverter categoryConverter;

    @Override
    public ProductBO convertToBO(ProductDO productDO) {

        ProductBO productBO = new ProductBO();

        productBO.setUid(productDO.getUid());
        productBO.setName(productDO.getName());
        productBO.setDescription(productDO.getDescription());
        productBO.setLongDescription(productDO.getLongDescription());
        productBO.setCategory(categoryConverter.convertToBO(productDO.getCategory()));
        productBO.setImageUrl(productDO.getImageUrl());
        productBO.setMrp(productDO.getMrp());
        productBO.setDiscount(productDO.getDiscount());
        productBO.setTags(new HashSet<>(productDO.getTags()));
        productBO.setQuantity(productDO.getQuantity());

        return productBO;
    }

    @Override
    public ProductDO convertToDO(ProductBO productBO) {

        ProductDO productDO = new ProductDO();

        productDO.setUid(productBO.getUid());
        productDO.setName(productBO.getName());
        productDO.setDescription(productBO.getDescription());
        productDO.setLongDescription(productBO.getLongDescription());
        productDO.setCategory(categoryConverter.convertToDO(productBO.getCategory()));
        productDO.setImageUrl(productBO.getImageUrl());
        productDO.setMrp(productBO.getMrp());
        productDO.setDiscount(productBO.getDiscount());

        Set<String> tags = productBO.getTags() == null ? Collections.EMPTY_SET : productBO.getTags();
        productDO.setTags(new ArrayList<>(tags));
        productDO.setQuantity(productBO.getQuantity());

        return productDO;
    }
}

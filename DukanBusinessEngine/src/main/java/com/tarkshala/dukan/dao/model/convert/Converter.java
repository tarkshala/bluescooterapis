package com.tarkshala.dukan.dao.model.convert;

import com.tarkshala.dukan.business.model.bo.EntityBO;
import com.tarkshala.dukan.dao.model.EntityDO;

public interface Converter<T extends EntityBO, R extends EntityDO> {

    T convertToBO(R entityDO);

    R convertToDO(T entityBO);
}

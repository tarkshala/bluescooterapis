package com.tarkshala.dukan.dao.model.convert;

import com.google.gson.Gson;
import com.tarkshala.dukan.business.model.bo.OrderBO;
import com.tarkshala.dukan.business.model.bo.ProductBO;
import com.tarkshala.dukan.dao.model.OrderDO;
import com.tarkshala.dukan.dao.model.ProductDO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class OrderConverter implements Converter<OrderBO, OrderDO>{

    @Autowired
    private AddressConverter addressConverter;

    @Autowired
    private ProductConverter productConverter;

    @Autowired
    private Logger logger;

    @Override
    public OrderBO convertToBO(OrderDO entityDO) {
        OrderBO order = new OrderBO();
        order.setDate(entityDO.getDate());
        order.setBuyerName(entityDO.getBuyerName());
        order.setPromotionalCode(entityDO.getPromotionalCode());
        order.setAddress(addressConverter.convertToBO(entityDO.getAddress()));

        List<ProductBO> products = new ArrayList<>();
        for (ProductDO productDO: entityDO.getProducts()) {
            products.add(productConverter.convertToBO(productDO));
        }
        order.setProducts(products);

        return order;
    }

    @Override
    public OrderDO convertToDO(OrderBO entityBO) {
        OrderDO order = new OrderDO();
        order.setBuyerName(entityBO.getBuyerName());
        order.setDate(entityBO.getDate());
        order.setPromotionalCode(entityBO.getPromotionalCode());

        logger.info(new Gson().toJson(entityBO.getAddress()));
        order.setAddress(addressConverter.convertToDO(entityBO.getAddress()));

        List<ProductDO> products = entityBO.getProducts().stream().map(productBO -> productConverter.convertToDO(productBO)).collect(Collectors.toList());
        order.setProducts(products);

        return order;
    }
}

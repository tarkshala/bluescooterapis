package com.tarkshala.dukan.dao;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.tarkshala.dukan.business.model.filter.Filter;
import com.tarkshala.dukan.config.Constants;
import com.tarkshala.dukan.dao.model.CategoryDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component(Constants.CATEGORY_DAO)
public class CategoryDAO extends EntityDAO<CategoryDO> {

    @Autowired
    private DynamoDBMapper dynamoDBMapper;

    @Override
    public Optional<CategoryDO> get(String id) {
        return Optional.ofNullable(dynamoDBMapper.load(CategoryDO.class, id));
    }

    @Override
    public List<CategoryDO> getAll() {
        return dynamoDBMapper.scan(CategoryDO.class, new DynamoDBScanExpression());
    }

    @Override
    public List<CategoryDO> get(List<Filter> filters, String previousPageLastKey, int count) {
        throw new UnsupportedOperationException("Filtering on categories is not supported yet.");
    }
}

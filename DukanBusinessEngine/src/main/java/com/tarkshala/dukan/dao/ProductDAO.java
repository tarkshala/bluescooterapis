package com.tarkshala.dukan.dao;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.google.gson.Gson;
import com.tarkshala.dukan.business.model.filter.Filter;
import com.tarkshala.dukan.config.Constants;
import com.tarkshala.dukan.dao.dynamoDB.FilterExpression;
import com.tarkshala.dukan.dao.model.ProductDO;
import com.tarkshala.dukan.dao.model.convert.ProductConverter;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component(Constants.PRODUCT_DAO)
public class ProductDAO extends EntityDAO<ProductDO> {

    @Autowired
    private Logger logger;

    @Autowired
    private DynamoDBMapper dynamoDBMapper;

    @Autowired
    private ProductConverter converter;

    @Override
    public Optional<ProductDO> get(String id) {
        return Optional.ofNullable(dynamoDBMapper.load(ProductDO.class, id));
    }

    @Override
    public List<ProductDO> getAll() {
        return dynamoDBMapper.scan(ProductDO.class, new DynamoDBScanExpression());
    }

    @Override
    public List<ProductDO> get(List<Filter> filters, String previousPageLastKey, int count) {

        FilterExpression filterExpression = new FilterExpression(filters);
        DynamoDBScanExpression scanExpression = new DynamoDBScanExpression()
                .withLimit(count)
                .withFilterExpression(filterExpression.getFilterExpression())
                .withExpressionAttributeNames(filterExpression.getAttributeNames())
                .withExpressionAttributeValues(filterExpression.getAttributeValues());

        logger.info(String.valueOf(scanExpression.getLimit()));
        logger.info(filterExpression.getFilterExpression());
        logger.info(new Gson().toJson(filterExpression.getAttributeNames()));
        logger.info(new Gson().toJson(filterExpression.getAttributeValues()));

        if (previousPageLastKey != null) {
            Map<String, AttributeValue> exclusiveStartKey = new HashMap<>();
            exclusiveStartKey.put(Constants.UID, new AttributeValue().withS(previousPageLastKey));
            scanExpression.setExclusiveStartKey(exclusiveStartKey);
        }

        List<ProductDO> products = new ArrayList<>();
        Iterator<ProductDO> iterator = dynamoDBMapper.scan(ProductDO.class, scanExpression).iterator();
        while (iterator.hasNext() && count > 0) {
            products.add(iterator.next());
            count--;
        }
        return products;
    }

}

package com.tarkshala.dukan.dao;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.tarkshala.dukan.business.model.filter.Filter;
import com.tarkshala.dukan.config.Constants;
import com.tarkshala.dukan.dao.dynamoDB.FilterExpression;
import com.tarkshala.dukan.dao.model.OrderDO;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class OrderDAO extends EntityDAO<OrderDO> {

    @Autowired
    private DynamoDBMapper dynamoDBMapper;

    @Setter
    private String tableName;

    @Override
    public void save(OrderDO entity) {
        DynamoDBMapperConfig.Builder builder = new DynamoDBMapperConfig.Builder().
                withTableNameOverride(DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(tableName));
        dynamoDBMapper.save(entity, builder.build());
    }

    @Override
    public void delete(OrderDO entity) {
        DynamoDBMapperConfig.Builder builder = new DynamoDBMapperConfig.Builder().
                withTableNameOverride(DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(tableName));
        dynamoDBMapper.delete(entity, builder.build());
    }

    @Override
    public void update(OrderDO entity) {
        DynamoDBMapperConfig.Builder builder = new DynamoDBMapperConfig.Builder().
                withTableNameOverride(DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(tableName));
        dynamoDBMapper.save(entity, builder.build());
    }

    @Override
    public Optional<OrderDO> get(String id) {
        return Optional.empty();
    }

    @Override
    public List<OrderDO> getAll() {

        DynamoDBMapperConfig.Builder builder = new DynamoDBMapperConfig.Builder().
                withTableNameOverride(DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(tableName));
        return dynamoDBMapper.scan(OrderDO.class, new DynamoDBScanExpression(), builder.build());
    }

    @Override
    public List<OrderDO> get(List<Filter> filters, String previousPageLastKey, int count) {

        FilterExpression filterExpression = new FilterExpression(filters);
        DynamoDBScanExpression scanExpression = new DynamoDBScanExpression()
                .withLimit(count)
                .withFilterExpression(filterExpression.getFilterExpression())
                .withExpressionAttributeNames(filterExpression.getAttributeNames())
                .withExpressionAttributeValues(filterExpression.getAttributeValues());

        if (previousPageLastKey != null) {
            Map<String, AttributeValue> exclusiveStartKey = new HashMap<>();
            exclusiveStartKey.put(Constants.UID, new AttributeValue().withS(previousPageLastKey));
            scanExpression.setExclusiveStartKey(exclusiveStartKey);
        }


        DynamoDBMapperConfig.Builder builder = new DynamoDBMapperConfig.Builder().
                withTableNameOverride(DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(tableName));

        List<OrderDO> orders = new ArrayList<>();
        Iterator<OrderDO> iterator = dynamoDBMapper.scan(OrderDO.class, scanExpression, builder.build()).iterator();
        while (iterator.hasNext() && count > 0) {
            orders.add(iterator.next());
            count--;
        }
        return orders;
    }
}

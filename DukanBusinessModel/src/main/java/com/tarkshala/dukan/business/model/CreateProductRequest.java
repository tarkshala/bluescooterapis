package com.tarkshala.dukan.business.model;

import com.tarkshala.dukan.business.model.bo.ProductBO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateProductRequest {

    private ProductBO productBO;
}

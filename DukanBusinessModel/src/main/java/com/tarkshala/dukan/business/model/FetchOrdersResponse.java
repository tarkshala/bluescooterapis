package com.tarkshala.dukan.business.model;

import com.tarkshala.dukan.business.model.bo.OrderBO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class FetchOrdersResponse {

    private List<OrderBO> orders;
}

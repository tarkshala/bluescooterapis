package com.tarkshala.dukan.business.model.bo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CategoryBO extends EntityBO {

    private String name;

    private String description;

    private String imageUrl;
}

package com.tarkshala.dukan.business.model.bo;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class EntityBO {

    private String uid;
}

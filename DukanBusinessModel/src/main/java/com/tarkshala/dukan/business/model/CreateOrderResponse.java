package com.tarkshala.dukan.business.model;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateOrderResponse {

    private String message;
}

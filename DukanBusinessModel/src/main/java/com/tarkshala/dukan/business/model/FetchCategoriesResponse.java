package com.tarkshala.dukan.business.model;

import com.tarkshala.dukan.business.model.bo.CategoryBO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FetchCategoriesResponse {

    private List<CategoryBO> categories;
}

package com.tarkshala.dukan.business.model;

import com.tarkshala.dukan.business.model.bo.ProductBO;
import lombok.*;

import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class FetchProductsResponse {

    @NonNull
    private List<ProductBO> products;
}

package com.tarkshala.dukan.business.model;

import com.tarkshala.dukan.business.model.filter.Filter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class FetchOrdersRequest {

    /**
     * Filters on different attributes of product.
     */
    private List<Filter> filters;

    /**
     * Start searching for product only after this key.
     */
    private String previousPageLastKey;

    /**
     * Number of products for this page.
     */
    private Integer pageSize;
}

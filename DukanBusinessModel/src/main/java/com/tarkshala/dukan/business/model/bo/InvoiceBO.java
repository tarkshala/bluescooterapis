package com.tarkshala.dukan.business.model.bo;

import lombok.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * @see OrderBO
 */
@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class InvoiceBO extends EntityBO {

    private int invoiceNumber;

    private long date;

    private String seller;

    private String buyer;

    private AddressBO buyerAddress;

    private String orderUid;

    private List<ProductBO> products;

    private BigDecimal discount;

    private BigDecimal taxes;

    private BigDecimal payableAmount;
}

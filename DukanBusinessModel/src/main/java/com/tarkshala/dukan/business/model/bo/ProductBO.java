package com.tarkshala.dukan.business.model.bo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductBO extends EntityBO {

    private String name;

    private String description;

    private String longDescription;

    private CategoryBO category;

    private String imageUrl;

    private float mrp;

    private float discount;

    private Set<String> tags;

    private float quantity;


}

package com.tarkshala.dukan.business.model.bo;

import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OrderBO extends EntityBO {

    private long date;

    private String buyerName;

    private AddressBO address;

    private List<ProductBO> products;

    private String promotionalCode;
}

package com.tarkshala.dukan.business.model;


import com.tarkshala.dukan.business.model.bo.CategoryBO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateCategoryRequest {

    private CategoryBO categoryBO;
}

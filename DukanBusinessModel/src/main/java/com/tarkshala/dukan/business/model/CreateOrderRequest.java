package com.tarkshala.dukan.business.model;

import com.tarkshala.dukan.business.model.bo.OrderBO;
import lombok.*;

@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CreateOrderRequest {

    private OrderBO order;
}

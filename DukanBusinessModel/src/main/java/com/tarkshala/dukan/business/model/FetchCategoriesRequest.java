package com.tarkshala.dukan.business.model;

import com.tarkshala.dukan.business.model.filter.Filter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class FetchCategoriesRequest {

    private List<Filter> filters;
}

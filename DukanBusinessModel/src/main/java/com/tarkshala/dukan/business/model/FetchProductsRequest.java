package com.tarkshala.dukan.business.model;

import com.tarkshala.dukan.business.model.filter.Filter;
import lombok.*;

import java.util.List;
import java.util.Optional;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FetchProductsRequest {

    /**
     * Filters on different attributes of product.
     */
    private List<Filter> filters;

    /**
     * Start searching for product only after this key.
     */
    private String previousPageLastKey;

    /**
     * Number of products for this page.
     */
    private Integer pageSize;
}

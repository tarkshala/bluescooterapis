package com.tarkshala.dukan.business.model.bo;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class AddressBO extends EntityBO{

    private String name;

    private String email;

    private String phone;

    private String address;
}
